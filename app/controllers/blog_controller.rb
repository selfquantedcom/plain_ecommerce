require 'json'

class BlogController < ApplicationController

   def home
      if user_signed_in?
         @navbar_props = {:active_item => "Home", user_id: current_user.id }   
      else
         @navbar_props = {:active_item => "Home", user_id: nil }   
      end
      @footer_props = {:active_item => "Home", :social_media_color => 'light'}
      article_items = []                 
      Article.all.order(created_at: :desc).each do |item|                   
         image = [url_for(item.images.first)]                  
         article_items.push({:item => item, :type => "Article", :image => image})
      end      
      @article_props = article_items.as_json            
      
      render template: 'blog/index'
   end

   def all_articles
      if user_signed_in?
         @navbar_props = {:active_item => "Articles", user_id: current_user.id }   
      else
         @navbar_props = {:active_item => "Articles", user_id: nil }   
      end      
      @footer_props = {:active_item => "Articles", :social_media_color => 'light'}

      article_items = []     
      Article.all.order(created_at: :desc).each do |item|                  
         image = url_for(item.images.first)        
         article_items.push({:item => item, :image => image})         
      end      

      @article_props = article_items.as_json   
      render template: "blog/articles/all_articles"
   end   

   def article_view
      if user_signed_in?
         @navbar_props = {:active_item => "", user_id: current_user.id }   
      else
         @navbar_props = {:active_item => "", user_id: nil }   
      end      
      @footer_props = {:social_media_color => 'light'}            
      article = Article.find(params[:id])
      @article_props = {:item => article, :type => "Article", :image => url_for(article.images.first)}.as_json   
      @comment_props = []
      comments = Comment.where(article_id: article.id).order(created_at: :desc)
      comments.each do |comment|
         user = User.find(comment.user_id)
         unless user.avatar.attached?
            @comment_props.push({:user_image => 'default_user.png', :comment => comment['comment_text'], :user => user})                               
         else
            @comment_props.push({:user_image => url_for(user.avatar), :comment => comment['comment_text'], :user => user})               
         end
      end
      render template: 'blog/articles/article_view'
   end

   def post_comment
      article_id = params[:article_id]
      comment = current_user.comments.new(comment_text: params['comment_text'], article_id: article_id)
      comment.save            
      article_title = Article.find(article_id)['title']
      redirect_to "/article_view/#{article_title}"
   end

   def portfolio
      if user_signed_in?
         @navbar_props = {:active_item => "Portfolio", user_id: current_user.id }   
      else
         @navbar_props = {:active_item => "Portfolio", user_id: nil }   
      end      
      @footer_props = {:active_item => "Portfolio", :social_media_color => 'light'}
      portfolio_items = []     
      PortfolioItem.all.each do |item|
         images = []
         item.images.each do |img| 
            images.push(url_for(img))
         end
         portfolio_items.push({:item => item, :images => images})
      end      
      @portfolio_props = portfolio_items.as_json
      render template: "blog/portfolio"
   end

   def products
      if user_signed_in?
         @navbar_props = {:active_item => "Products", user_id: current_user.id }   
      else
         @navbar_props = {:active_item => "Products", user_id: nil }   
      end      
      @footer_props = {:active_item => "Products", :social_media_color => 'light'}
      
      product_items = []     
      Product.all.each do |item|         
         images = []
         item.images.each do |img| 
            images.push(url_for(img))
         end
         product_items.push({:item => item, :type => "Product", :images => images})         
      end      

      @product_props = product_items.as_json      
      
      
      render template: "blog/products"
   end

   def product_view
      if user_signed_in?
         @navbar_props = {:active_item => "", user_id: current_user.id }   
      else
         @navbar_props = {:active_item => "", user_id: nil }   
      end      
      @footer_props = {:active_item => "Products", :social_media_color => 'light'}
      product_id = params["id"]
      
      product = Product.find(product_id)
      product_items = []
      images = []        
      product.images.each do |img|                   
         images.push(url_for(img))
      end
      @product_props = {:item => product, :type => "Product", :images => images}.as_json                        
      render template: "blog/product_view"
   end

   def profile_page      
      unless user_signed_in? && current_user.id == params[:id].to_i
         redirect_to '/'         
      else      
         @navbar_props = {:active_item => "Profile Page", user_id: current_user.id }       
         @footer_props = {:active_item => "Account Management", :social_media_color => 'light'}    

         if current_user.avatar.attached?
            @profile_picture = url_for(current_user.avatar)
         else
            @profile_picture = 'default_user.png'
         end
         render template: "blog/profile_page"
      end
   end

   def update_profile      
      user = User.find(params[:id])
      avatar = params["user"]["avatar"]
      if user && avatar
         user.avatar.attach(avatar)
      end         
      redirect_to "/profile_page/#{user.id}"
   end

   def newsletter_signup
      redirect_to '/'
   end

end
