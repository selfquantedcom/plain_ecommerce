class BlogManagementController < ApplicationController
   before_action :authenticate_admin!   
   
   def dashboard
      render template: 'blog/user_dashboard'
   end

   def article_new                
      render template: 'blog/articles/create_article'
   end

   def article_create
      title = params["article"]["title"]
      introduction = params["article"]["introduction"]
      body = params["article"]["body"]
      image = params["article"]["image"]
      article = current_user.articles.new(title: title, introduction: introduction, body: body)
      article.save
      if image
         article.images.attach(image)
      end
      render template: 'blog/articles/create_article'
   end

   def product_new
      render template: 'blog/create_product'
   end

   def product_create
      title = params["product"]["title"]
      introduction = params["product"]["introduction"]
      body = params["product"]["body"]
      image = params["product"]["image"]
      product = current_user.products.new(title: title, introduction: introduction, body: body)
      product.save
      if image
         product.images.attach(image)
      end
      render template: 'blog/create_product'
   end

   def portfolio_item_new
      render template: 'blog/create_portfolio_item'
   end

   def portfolio_item_create
      title = params["portfolio"]["title"]
      introduction = params["portfolio"]["introduction"]
      body = params["portfolio"]["body"]
      preview_image = params["portfolio"]["preview_image"]
      modal_image = params["portfolio"]["modal_image"]
      portfolio_item = current_user.portfolio_items.new(title: title, introduction: introduction, body: body)
      portfolio_item.save
      if preview_image && modal_image
         portfolio_item.images.attach(preview_image)
         portfolio_item.images.attach(modal_image)
      end
      render template: 'blog/create_portfolio_item'
   end   

end