# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  respond_to :html, :js
  before_action :require_no_authentication, only: :cancel
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    @navbar_props = {:active_item => "Account Management"}
    @footer_props = {:active_item => "Account Management", :social_media_color => 'light'}            
    super
  end

  # POST /resource/sign_in
  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_navigational_format?
    sign_in(resource_name, resource)
    if !session[:return_to].blank?            
      session[:return_to] = nil
      redirect_to '/admin'      
    else      
      redirect_to '/admin'      
    end    
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
