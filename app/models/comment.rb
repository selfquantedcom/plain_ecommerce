class Comment < ApplicationRecord
   validates :comment_text, :article_id, :user_id, presence: true   
   belongs_to :user
   belongs_to :article   
end
