class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  validates :first_name, :last_name, presence: true   
  validates :email, uniqueness: true   
  has_many :articles
  has_many :comments
  has_many :portfolio_items
  has_many :products
  has_one_attached :avatar
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable
end
