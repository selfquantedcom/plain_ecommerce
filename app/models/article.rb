class Article < ApplicationRecord
   validates_uniqueness_of :title
   belongs_to :user
   has_many :comments
   has_many_attached :images
end
