import React from 'react';

class BlogNavbar extends React.Component {
  render () {
    return (           
      <nav className="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
      <div className="navbar-brand"></div>                      
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
      <ul className="navbar-nav bg-dark ">
      <li className="nav-item ">
      <a href="/" className={this.props.active_item == "Home" ? "nav-link active" : "nav-link" }>Home</a>              
      </li>      
      <li className="nav-item ">
      <a href="/all_articles" className={this.props.active_item == "Articles" ? "nav-link active" : "nav-link" }>Articles</a>              
      </li>                  
      <li className="nav-item ">
      <a href="/products" className={this.props.active_item == "Products" ? "nav-link active" : "nav-link" }>Products</a>              
      </li>                  
      <li className="nav-item ">
      <a href="/portfolio" className={this.props.active_item == "Portfolio" ? "nav-link active" : "nav-link" }>Portfolio</a>                            
      </li>      
      {this.props.user_id ?
        <>
        <li className="nav-item ">      
        <a href={'/profile_page/' + this.props.user_id} className={this.props.active_item == "Profile Page" ? "nav-link active" : "nav-link" }><i className="fa fa-user"></i>

        </a>                            
        </li>
        <li className="nav-item ">             
        <a href="/blog_sign_out" className="nav-link" alt="Sign out"><i className="fa fa-sign-out"></i></a>                            
        </li>
        </>
        :  
        <>      
        <li className="nav-item ">      
        <a href="/blog_login" className={this.props.active_item == "Account Management" ? "nav-link active" : "nav-link" }>Sign In</a>                            
        </li>        
        </>
      }
      </ul>
      </div>  
      </nav>      
      )
  }
}

export default BlogNavbar