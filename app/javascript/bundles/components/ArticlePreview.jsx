import React from 'react';

class ArticlePreview extends React.Component {
  render () {
    return ( 
          <React.Fragment>                   
            <a href={"/article_view/" + this.props.item.id}> 
              <div className="card h-100 w-100">
                <img className="img-responsive img-fluid card-img-top img-preview" src={this.props.image}/ >            
                <div className="card-body item-backgorund text-left card-block d-flex flex-column">
                  <h4 className="card-title text-black">{this.props.item.title}</h4>             
                </div>
              </div>  
            </a>                           
          </React.Fragment>        
      )
  }
}

export default ArticlePreview