import React from 'react';

class PortfolioModal extends React.Component {
  
  createMarkup = (htmlString) => {
    return {__html: htmlString};
  }

  render () {
    return ( 
      <div className="card h-100">               
         <img className="img-responsive card-img-top portfolio-img-preview" src={this.props.images[0]}/>               
         <div className="card-body item-backgorund card-block d-flex flex-column">
            <h4 className="card-title">{this.props.item.title}</h4>
            <p className="card-text">{this.props.item.introduction}</p>                  
            <button type="button" className="btn btn-secondary btn-lg btn-block text-truncate mt-auto" data-toggle="modal" data-target={"#portfolio_modal_" + this.props.item.id}> View       
            </button>                        
            <div className="modal" id={"portfolio_modal_" + this.props.item.id}>
              <div className="modal-dialog">
                <div className="modal-content">              
                  <div className="modal-header">
                    <h4 className="modal-title">{this.props.item.title}</h4>
                    <span data-dismiss="modal" className="modal-close-btn">
                      <span aria-hidden="true">&times;</span>
                    </span>
                  </div>                     
                  <div className="modal-body"> 
                  <div className="container">
                    <div className="row justify-content-center text-center centered-content mb-3">                               
                      <img className="portfolio-item-view-img img-responsive img-fluid" src={this.props.images[1]}/>
                    </div>
                    <div className="row">
                      <div className="container">
                    <div dangerouslySetInnerHTML={this.createMarkup(this.props.item.body)}/>
                    </div>
                    </div>
                    </div>

                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>        
                </div>
              </div>
            </div>
         </div>
      </div>      
      )
  }
}

export default PortfolioModal