import React from 'react';

class ArticlePreviewLarge extends React.Component {

  truncateText = (str, maxLen, separator = ' ') => {
    if (str.length <= maxLen) {
      return str;
    }
    return str.substr(0, str.lastIndexOf(separator, maxLen)) + ' ...';    
  }

  render () {
    return (                  
        <div className="card mb-4">
        <img className="card-img-top img-large-preview" src={this.props.image} alt="article preview img"/>
        <div className="card-body">
          <h2 className="card-title">{this.props.item.title}</h2>
          <p className="card-text">{this.truncateText(this.props.item.introduction, 250)}</p>
          <a className="btn btn-secondary col-12 col-md-3" href={"article_view/" + this.props.item.title}>Read More &rarr;</a>
        </div>
        <div className="card-footer text-muted">
          <span> </span>
          <a href="/portfolio">Frantisek Friedl</a>
        </div>
      </div>
      )
  }
}

export default ArticlePreviewLarge