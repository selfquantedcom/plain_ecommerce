import React from 'react';
import SocialMedia from './SocialMedia';

class BlogFooter extends React.Component {
  render () {
    return ( 
      <footer className={this.props.active_item == 'Account Management' || this.props.active_item == 'Products' ? 'navbar navbar-expand-sm bg-dark navbar-dark fixed-bottom' : 'navbar navbar-expand-sm bg-dark navbar-dark'}>         
         <div className="container text-center justify-content-center m">                  
            <div className="row full-width mt-4">
              <div className="col-12">Copyright © 2020, Frantisek Friedl</div>                           
              <SocialMedia {...this.props}/> 
            </div>
         </div>         
      </footer>
      )
  }
}

export default BlogFooter
