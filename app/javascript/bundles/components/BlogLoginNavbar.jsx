import React from 'react';

class BlogLoginNavbar extends React.Component {
  render () {
    return ( 
    <div>
       <a href="/blog_login" className={"col-6 btn-dark btn btn-outline-light float-left mt-2 mb-2"}>Sign In</a>              
       <a href="/blog_sign_up" className={"col-6 btn-dark btn btn-outline-light float-right mt-2 mb-2"}>Sign Up</a>           
    </div>      
      )
  }
}

export default BlogLoginNavbar