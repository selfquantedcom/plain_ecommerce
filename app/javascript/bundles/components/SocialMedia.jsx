import React from 'react';

class SocialMedia extends React.Component {
  render () {
    return (       
        <div className="row justify-content-center full-width social-media-links mt-2 mb-4">
          <div className="col-12">
            <ul className="list-inline centered-content">               
               <li className="list-inline-item">
                  <a href='https://www.linkedin.com/in/frantisek-friedl/' target="_blank" className={'btn btn-outline-' + this.props.social_media_color + ' btn-social text-center rounded-circle'}><i className="fa fa-linkedin"></i></a>
               </li>       
               <li className="list-inline-item">
                  <a href='https://github.com/FrantaF' target="_blank" className={'btn btn-outline-' + this.props.social_media_color + ' btn-social text-center rounded-circle'}><i className="fa fa-github"></i></a>
               </li>       
               <li className="list-inline-item">
                  <a href='https://bitbucket.org/account/user/selfquantedcom/projects/PRF' target="_blank" className={'btn btn-outline-' + this.props.social_media_color + ' btn-social text-center rounded-circle'}><i className="fa fa-bitbucket"></i></a>
               </li>                            
            </ul>                
          </div>  
        </div>         
      )
  }
}

export default SocialMedia
