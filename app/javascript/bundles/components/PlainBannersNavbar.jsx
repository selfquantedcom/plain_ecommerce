import React from 'react';

class PlainBannersNavbar extends React.Component {
  render () {
    return ( 
    <div>
       <a href="/plain_banners/all_banners" className="col-6 btn-dark btn btn-outline-light float-left mt-2 mb-2">All banners</a>              
       <a href="/plain_banners" className="col-6 btn-dark btn btn-outline-light float-right mt-2 mb-2">Create banner</a>           
    </div>      
      )
  }
}

export default PlainBannersNavbar