import ReactOnRails from 'react-on-rails';

import ArticlePreview from '../bundles/components/ArticlePreview';
import BlogNavbar from '../bundles/components/BlogNavbar';
import BlogFooter from '../bundles/components/BlogFooter';
import PlainBannersNavbar from '../bundles/components/PlainBannersNavbar';
import BlogLoginNavbar from '../bundles/components/BlogLoginNavbar';
import ArticlePreviewLarge from '../bundles/components/ArticlePreviewLarge';
import SocialMedia from '../bundles/components/SocialMedia';

ReactOnRails.register({    
  ArticlePreview,  
  BlogNavbar,
  BlogFooter,
  PlainBannersNavbar,
  BlogLoginNavbar,  
  ArticlePreviewLarge,
  SocialMedia  
});
