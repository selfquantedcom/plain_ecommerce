class RenameEmailFromConsent < ActiveRecord::Migration[5.2]
  def change
   rename_column :consents, :email, :user_email
end
end
