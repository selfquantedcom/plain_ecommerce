class RenameArticleDbColumn < ActiveRecord::Migration[5.2]
  def change
   rename_column :articles, :body_text, :body

  end
end
