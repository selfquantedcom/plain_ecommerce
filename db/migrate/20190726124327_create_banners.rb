class CreateBanners < ActiveRecord::Migration[5.2]
  def change
    create_table :banners do |t|    
      t.integer :shop_id, :null => false   
      t.string :script_tag_id, default: nil
      t.string :record_name, :nil => false, unique: true       
      t.string :color 
      t.string :title 
      t.string :body_text 
      t.timestamps
    end
  end
end
