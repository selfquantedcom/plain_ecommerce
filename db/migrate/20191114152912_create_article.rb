class CreateArticle < ActiveRecord::Migration[5.2]
   
  def change
    create_table :articles do |t|
      t.string :main_title
      t.text :body_text
    end
  end
end
