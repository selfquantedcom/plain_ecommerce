class ChangeArticles < ActiveRecord::Migration[5.2]
  def change
    rename_column :articles, :main_title, :title
     add_column :articles, :email, :string
  end
end

  
