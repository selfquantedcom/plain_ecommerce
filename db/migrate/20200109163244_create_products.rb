class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.integer :user_id
      t.string :title
      t.string :introduction
      t.string :body
      t.timestamps
    end
  end
end
