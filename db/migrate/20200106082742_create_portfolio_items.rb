class CreatePortfolioItems < ActiveRecord::Migration[5.2]
  def change
    create_table :portfolio_items do |t|
      t.integer :user_id
      t.string :title
      t.string :introduction
      t.string :body
      t.timestamps
    end
  end
end
