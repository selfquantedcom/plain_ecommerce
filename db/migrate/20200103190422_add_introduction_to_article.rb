class AddIntroductionToArticle < ActiveRecord::Migration[5.2]
  def change
   add_column :articles, :introduction, :string   
  end
end
