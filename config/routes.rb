  Rails.application.routes.draw do

  #Blog
  root :to => 'blog#home'
  get '/all_articles' => 'blog#all_articles'
  get '/article_view/:id' => 'blog#article_view' 
  post '/blog/post_coment/:article_id' => 'blog#post_comment' 
  get '/portfolio' => 'blog#portfolio'   
  get '/products' => "blog#products"
  get '/product_view/:id' => "blog#product_view"
  get '/profile_page/:id' => "blog#profile_page" 
  post '/update_profile/:id' => "blog#update_profile"  

  # Blog management by admin  
  get '/create_article' => "blog_management#article_new"
  post '/create_article' => "blog_management#article_create"
  get '/create_portfolio_item' => "blog_management#portfolio_item_new"
  post '/create_portfolio_item' => "blog_management#portfolio_item_create"
  get '/create_product' => "blog_management#product_new"
  post '/create_product' => "blog_management#product_create"

  # Devise
  devise_for :users, controllers: { sessions: 'sessions' }
  devise_scope :user do
    post "/blog_login" => "users/sessions#create"    
    get '/blog_login' => 'users/sessions#new'    
    get '/blog_sign_up' => 'users/registrations#new'
    post '/blog_sign_up' => 'users/registrations#create'
    delete '/blog_sign_out' => 'users/sessions#destroy'            

    # temporary for testing
    get '/blog_sign_out' => 'users/sessions#destroy'   
    # END
  end
  # Devise End
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'  
  # Blog END  

end
