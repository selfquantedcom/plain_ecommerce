--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: active_storage_attachments; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.active_storage_attachments (
    id bigint NOT NULL,
    name character varying NOT NULL,
    record_type character varying NOT NULL,
    record_id bigint NOT NULL,
    blob_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.active_storage_attachments OWNER TO root;

--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.active_storage_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.active_storage_attachments_id_seq OWNER TO root;

--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.active_storage_attachments_id_seq OWNED BY public.active_storage_attachments.id;


--
-- Name: active_storage_blobs; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.active_storage_blobs (
    id bigint NOT NULL,
    key character varying NOT NULL,
    filename character varying NOT NULL,
    content_type character varying,
    metadata text,
    byte_size bigint NOT NULL,
    checksum character varying NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.active_storage_blobs OWNER TO root;

--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.active_storage_blobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.active_storage_blobs_id_seq OWNER TO root;

--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.active_storage_blobs_id_seq OWNED BY public.active_storage_blobs.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO root;

--
-- Name: articles; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.articles (
    id bigint NOT NULL,
    title character varying,
    body text,
    user_id integer,
    introduction character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.articles OWNER TO root;

--
-- Name: articles_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_id_seq OWNER TO root;

--
-- Name: articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.articles_id_seq OWNED BY public.articles.id;


--
-- Name: banners; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.banners (
    id bigint NOT NULL,
    shop_id integer NOT NULL,
    script_tag_id character varying,
    record_name character varying,
    color character varying,
    title character varying,
    body_text character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.banners OWNER TO root;

--
-- Name: banners_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.banners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banners_id_seq OWNER TO root;

--
-- Name: banners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.banners_id_seq OWNED BY public.banners.id;


--
-- Name: portfolio_items; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.portfolio_items (
    id bigint NOT NULL,
    user_id integer,
    title character varying,
    introduction character varying,
    body character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.portfolio_items OWNER TO root;

--
-- Name: portfolio_items_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.portfolio_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.portfolio_items_id_seq OWNER TO root;

--
-- Name: portfolio_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.portfolio_items_id_seq OWNED BY public.portfolio_items.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.products (
    id bigint NOT NULL,
    user_id integer,
    title character varying,
    introduction character varying,
    body character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.products OWNER TO root;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO root;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO root;

--
-- Name: shops; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.shops (
    id bigint NOT NULL,
    shopify_domain character varying NOT NULL,
    shopify_token character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.shops OWNER TO root;

--
-- Name: shops_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.shops_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shops_id_seq OWNER TO root;

--
-- Name: shops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.shops_id_seq OWNED BY public.shops.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    admin boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO root;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: active_storage_attachments id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.active_storage_attachments ALTER COLUMN id SET DEFAULT nextval('public.active_storage_attachments_id_seq'::regclass);


--
-- Name: active_storage_blobs id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.active_storage_blobs ALTER COLUMN id SET DEFAULT nextval('public.active_storage_blobs_id_seq'::regclass);


--
-- Name: articles id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.articles ALTER COLUMN id SET DEFAULT nextval('public.articles_id_seq'::regclass);


--
-- Name: banners id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.banners ALTER COLUMN id SET DEFAULT nextval('public.banners_id_seq'::regclass);


--
-- Name: portfolio_items id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.portfolio_items ALTER COLUMN id SET DEFAULT nextval('public.portfolio_items_id_seq'::regclass);


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: shops id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.shops ALTER COLUMN id SET DEFAULT nextval('public.shops_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: active_storage_attachments; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.active_storage_attachments (id, name, record_type, record_id, blob_id, created_at) FROM stdin;
1	images	Article	5	1	2020-01-03 09:34:03.480605
2	images	PortfolioItem	3	2	2020-01-06 18:11:00.776984
3	images	PortfolioItem	3	3	2020-01-06 18:11:00.994168
4	images	PortfolioItem	4	4	2020-01-07 17:26:15.960962
5	images	PortfolioItem	4	5	2020-01-07 17:26:16.137213
6	images	PortfolioItem	5	6	2020-01-07 17:30:36.739973
7	images	PortfolioItem	5	7	2020-01-07 17:30:36.871044
8	images	PortfolioItem	2	8	2020-01-07 17:32:13.98787
9	images	PortfolioItem	2	9	2020-01-07 17:32:55.344488
10	images	PortfolioItem	1	10	2020-01-07 17:34:51.424397
11	images	PortfolioItem	1	11	2020-01-07 17:35:19.268333
14	images	Product	1	14	2020-01-09 17:28:46.535196
15	images	Article	13	15	2020-01-09 17:35:20.228381
16	images	Product	2	16	2020-01-10 08:46:07.360025
17	images	PortfolioItem	7	17	2020-01-11 15:01:02.942858
18	images	PortfolioItem	7	18	2020-01-11 15:01:16.862244
\.


--
-- Data for Name: active_storage_blobs; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.active_storage_blobs (id, key, filename, content_type, metadata, byte_size, checksum, created_at) FROM stdin;
1	xoRw2FAF2nwqoHJfNLmorA3w	welcome-cat-img.jpg	image/jpeg	{"identified":true,"analyzed":true}	25447	Zfgn8oddPBh1bN9hWAQpmA==	2020-01-03 09:34:03.397759
2	XTYJJn7zh1QAEvEAgimnXmnk	binberries.PNG	image/png	{"identified":true,"analyzed":true}	3303284	Fl//vLYci34uTXCZs7EXfw==	2020-01-06 18:11:00.694853
3	nw3hcQiguXXd9RZCjnNVLzyq	binberries.PNG	image/png	{"identified":true,"analyzed":true}	3303284	Fl//vLYci34uTXCZs7EXfw==	2020-01-06 18:11:00.962495
4	8PrUuJ3BBwkNS4sz1xod8XGY	IreenWebsite.PNG	image/png	{"identified":true,"analyzed":true}	202353	PKWyq0SxZGaMTTd1GiABlQ==	2020-01-07 17:26:15.91122
5	nX2RxmTg6En9MaeMyJYztTuR	IreenWebsite.PNG	image/png	{"identified":true,"analyzed":true}	202353	PKWyq0SxZGaMTTd1GiABlQ==	2020-01-07 17:26:16.098627
6	i37FYSSZ3xnwb52iUv7KcsPb	dentistry.PNG	image/png	{"identified":true,"analyzed":true}	1246470	XgmQIrGuZTlCgYIFWMoepA==	2020-01-07 17:30:36.674212
7	KenwPufcQFGY9N3ywsPk1ToM	dentistry.PNG	image/png	{"identified":true,"analyzed":true}	1246470	XgmQIrGuZTlCgYIFWMoepA==	2020-01-07 17:30:36.818395
8	tyFf77YTxHHeG7pxLWqqhm3y	URSS2_intro.jpg	image/jpeg	{"identified":true,"analyzed":true}	791427	Drxs2JwT1YNSno+i1fe9Ww==	2020-01-07 17:32:13.964635
9	ULSd1ubpfAzzSMGwDLTzUxW4	URSS2.jpg	image/jpeg	{"identified":true,"analyzed":true}	1243122	MmuygViY/a2xMv/Mneywrg==	2020-01-07 17:32:55.306504
10	YzvfL8yZMZhuJ64a6LsPE4PF	URSS_intro.jpg	image/jpeg	{"identified":true,"analyzed":true}	293503	PFQmUm5CAX+I0WxhECR+EA==	2020-01-07 17:34:51.376804
11	Y7nRWyTSeY4v8ZGw2WjfjVtr	URSS.jpg	image/jpeg	{"identified":true,"analyzed":true}	927617	p9d3er9ST/Yx23YnzcF7zg==	2020-01-07 17:35:19.235288
14	YEGKvB4jAbEZcpiVjZMxhPPe	chrome_plugin.png	image/png	{"identified":true,"analyzed":true}	31607	RSzOjCilBD2OZNryuNVOGw==	2020-01-09 17:28:46.438785
15	boVpWdj1XxuPJbFcphZtyHsq	article_1_img_preview.png	image/png	{"identified":true,"analyzed":true}	98399	RLLEWuLI6+nLvfQ4Iy48rA==	2020-01-09 17:35:20.190714
16	NKmzmgBeRGbPZkDbHhA3PvdN	plain_banners_preview.png	image/png	{"identified":true,"analyzed":true}	16627	70dY6XsHdnAM5IFF+Lbw9w==	2020-01-10 08:46:07.313858
17	fqdiish1ZmNqEQEreymjsZXs	sample_weblayer.jpg	image/jpeg	{"identified":true,"analyzed":true}	22855	+d4aa41ustGjDhUNFMxZ2w==	2020-01-11 15:01:02.765312
18	1uZNWED6aCRRX68nQVffRL2g	sample_weblayer.jpg	image/jpeg	{"identified":true,"analyzed":true}	22855	+d4aa41ustGjDhUNFMxZ2w==	2020-01-11 15:01:16.811641
\.


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2019-11-11 19:37:53.757015	2019-11-11 19:37:53.757015
\.


--
-- Data for Name: articles; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.articles (id, title, body, user_id, introduction, created_at, updated_at) FROM stdin;
13	Creating responsive emails with MJML	  \r\n  <p>MJML can either be used locally with Node Package Manager or with a standalone online text editor with life preview.</p>\r\n  <ul class="list-group mb-2">\r\n    <li class="list-group-item"><a href="https://mjml.io/try-it-live" target="_blank">Editor</a></li>          \r\n    <li class="list-group-item"><a href="https://mjml.io/documentation/" target="_blank">Documentation</a></li>\r\n    <li class="list-group-item"><a href="https://mjml.io/templates" target="_blank">Sample templates</a></li>\r\n  </ul>\r\n  <h2>Walkthrough example</h2>         \r\n  <p>MJML utilizes a grid system. &lt;mj-section&gt; enables us to put content into a row whereas &lt;mj-column&gt; enables us to display content next to each other (i.e. in columns) in a particular row. Width of the columns is shared evenly. E.g. with 2 columns each will occupy 50%. Default width of a row is 600px. </p>\r\n  <p> To insert custom HTML elements we would use the MJML tag &lt;mj-text&gt;. Within this tag we can specify our own HTML elements. Let’s look at an example. Say we want to display an image of a product with a URL, product description and product price all next to each other. Our code could look something like this.</p>\r\n  <div class="justify-content-center mt-3 mb-3">\r\n   <img class="img-fluid" src="/assets/article_1_img_1.png" alt="MJML code"/>\r\n </div>\r\n <p>First we create a row by using &lt;mj-section&gt; tag. Within this tag we then create 3 columns using &lt;mj-column&gt;. As our description of a product is much longer than the price of a product we decide to allocate more space to this column specifying attribute width &lt;mj-column width=”250”&gt; Notice that we don’t add units (e.g. pixels) and that the total width of all columns adds up to 600. For other MJML attributes refer to the documentation. Those shown in this example should be largely self-explanatory. To create URL links and to embed these into a picture we need to use classic HTML. This we can do by nesting our HTML code in the &lt;mj-text&gt; tag.</p>\r\n <div class="row justify-content-center mt-3 mb-3">\r\n  <img class="img-fluid" src="/assets/article_1_img_2.png" alt="MJML code"/>\r\n</div>\r\n<p>Notice, that we were able to use a very straight forward syntax while MJML ensures that the HTML code will display correctly on various browsers and email clients (i.e. the compiled HTML will be garbage code). Typically we would need to write a complicated structure with several nested HTML tables to ensure a correct display particularly in Outlook. </p>\r\n<p>In the above example we have used in-line styling but we can also write our CSS code in the “head section” as follows: </p>\r\n<div class="justify-content-center mt-3 mb-3">\r\n  <img class="img-fluid" src="/assets/article_1_img_3.png" alt="MJML code" />\r\n</div>\r\n<p>Note that the CSS will apply only to HTML elements and will have no impact on the MJML tags. To style MJML tags we need to use the supported attributes.</p>	5	MJML is a framework for developing responsive email templates which render correctly across a wide range of email providers web browsers. Hey, it is even compatible with Outlook!	2020-01-03 19:23:37.066145	2020-01-09 17:35:20.233252
\.


--
-- Data for Name: banners; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.banners (id, shop_id, script_tag_id, record_name, color, title, body_text, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: portfolio_items; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.portfolio_items (id, user_id, title, introduction, body, created_at, updated_at) FROM stdin;
3	5	BinBerries	Application for the optimization of waste collection leveraging the power of analytics, Google maps API as well as a custom-made API for communication with waste-level measuring endpoints.	<p> • Developed a full fledged application as a member of a team of 3; during a Programming Bootcamp.</p>\r\n                                            <p> • Responsible for the creation of an initial application prototype and creation and integration of external APIs including payment processing.</p>\r\n                                            <p> • Front-End technologies: HTML, CSS, JavaScript, Bootstrap.</p>  \r\n                                            <p> • Back-End: Ruby (Rails), Python (Flask) for an external API, PostgreSQL.</p>                            \r\n                                            <p> • Integration: Git, Google API and others, Heroku</p>\r\n                                            <p> • Purpose of the application: An external sonic sensor determines the fill level of a container. This is then transmitted via an API to the core application and displayed on a map. It is possible to dynamically adjust collection route by excluding any container bellow its thrashold capacity whereby reducing waste collection costs. The application further enables advanced data tracking, visualisation and analytics to further optimise the logistical operation.</p>\r\n                                            <p> • Won a first place at a finalist competition.</p>\r\n\r\n                                            <p> • Available at: <a href="https://aqueous-beyond-47975.herokuapp.com/" target="_blank ">BinBerries</a></p>\r\n                                            <p> • Login: admin, qwerasdf </p>	2020-01-06 18:11:00.520737	2020-01-06 18:11:00.995459
1	5	Option Pricing	Assesment of the impact of volaitility estimators on option pricing	<p> • Received £1000 grant to research Comparison of volatility estimators: from the perspective of option pricing.</p>                                                       <p> • Gathered and cleaned data using DataStream and Python programming language.</p>                                                      <p> • Analysed financial data using Excel and MATLAB. </p>                                                     <p> • Concluded that the use of realized volatility results in a 38% smaller price forecasting error on average and presented the findings in a research poster.</p>	2020-01-06 08:47:32.808412	2020-01-07 17:35:19.2716
5	5	Dental Surgery	Website for a successful dental surgery operating in the center of Prague	<p> • Gathered functional requirements from the client.</p>\r\n<p> • Coded a responsive portfolio website using HTML, CSS, Bootstrap and JavaScript and deployed this to the production server.</p>	2020-01-07 17:30:36.462241	2020-01-07 17:30:36.872698
4	5	Ching Yan Hung	A portfolio website for a successful journalist in Hong Kong	<p> • Coded a responsive portfolio website using HTML, CSS, Bootstrap and JavaScript based on an existing template.</p>                                            <p> • Deployed the site to a Linux server running Apache.</p>  <p> • Available at: <a href="http://chingyanhung.tk" target="_blank ">chingyanhung.tk</a></p>	2020-01-07 17:26:15.760581	2020-01-07 18:16:27.214599
2	5	Quantitative Investing	Designing statistically robust investment algorithms arrived at by data mining carried out using Evolutionary Algorithms	<p> • Received £1000 grant to research: The use of evolutionary algorithms in the design of investment systems. </p>                                                        <p> • Gathered and cleaned financial data using Amibroker and IQFeed.</p>                                                     <p> • Wrote a datamining script in AFL language, carried out datamining and analysed its results.</p>                                                      <p> • Designed an investment algorithm with statistically proven robustness, generating abnormal returns: mean Sharpe Ratio of 3.31 and mean Recovery Factor of 3.25 across samples.</p>                                                      <p> • The research poster was showcased at the International Conference of Undergraduate Research at the University of Warwick.</p>	2020-01-06 14:34:34.958715	2020-01-07 17:32:55.34971
7	5	Weblayers	Responsive modifications of clients' UI	<p>Modifying clients' UI, preparing Weblayers and tracking customer activity</p>	2020-01-11 15:01:02.918104	2020-01-11 15:01:16.865574
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.products (id, user_id, title, introduction, body, created_at, updated_at) FROM stdin;
2	5	Plain Banners	Shopify plugin enabling you to inject banners into your store front.	Create banners with a custom text and display them on the front of your Shopify store. Simple and Convenient.	2020-01-10 08:46:07.050067	2020-01-10 08:46:07.362799
1	5	LinkedIn Auto Engagement	Google Chrome extension enabling you to automatically endorse affiliated content	<p>.</p>	2020-01-09 17:28:46.150156	2020-01-12 18:37:44.170357
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.schema_migrations (version) FROM stdin;
20190711105235
20190726124327
20191114152912
20200102091842
20200102152002
20200103083108
20200103083349
20200103085522
20200103092615
20200103190422
20200103192019
20200106082742
20200109163244
20200110083710
\.


--
-- Data for Name: shops; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.shops (id, shopify_domain, shopify_token, created_at, updated_at) FROM stdin;
1	plainecommerce.myshopify.com	1d9a6fa0dc5513367e46b1e39305abfd	2019-11-11 19:38:29.693257	2019-12-31 17:31:00.99707
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, created_at, updated_at, admin) FROM stdin;
1	me@mail.com	$2a$11$5rsIhvSBsizRI9DU8I.A4uXFFjTr1.QVeQLjuuDWe.Gl/oxDAX3KK	\N	\N	\N	2020-01-02 09:50:03.719262	2020-01-02 09:50:03.719262	f
2	test@email.com	$2a$11$nCDL6wd8OkTt0QNNe8aM2O1I4w.l6H6mfTRBNS/Wp/WStsxJvLOBK	\N	\N	\N	2020-01-02 13:12:08.065322	2020-01-02 13:12:08.065322	f
3	test2@email.com	$2a$11$KpyNJOOYJlv1QzANN9l8/OxVhSTg.p6chKq//hMz9gvIYdDVbjoGW	\N	\N	\N	2020-01-02 13:15:46.575602	2020-01-02 13:15:46.575602	f
4	new@email.com	$2a$11$vMzyoPQuYZB3tIs.QsB6j.rIyc.IyydbTTypV8mfSSuMd4Z7au7a2	\N	\N	\N	2020-01-02 15:26:55.972625	2020-01-02 15:26:55.972625	f
5	franta@email.com	$2a$11$.q8BJptZaPZlC17Gx1V2re5W1X1TVeCRIP9CZ41jHcGXoKZ6vI5A.	\N	\N	\N	2020-01-02 15:37:04.225169	2020-01-02 15:37:04.225169	t
\.


--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.active_storage_attachments_id_seq', 18, true);


--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.active_storage_blobs_id_seq', 18, true);


--
-- Name: articles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.articles_id_seq', 13, true);


--
-- Name: banners_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.banners_id_seq', 1, true);


--
-- Name: portfolio_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.portfolio_items_id_seq', 7, true);


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.products_id_seq', 2, true);


--
-- Name: shops_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.shops_id_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_id_seq', 5, true);


--
-- Name: active_storage_attachments active_storage_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT active_storage_attachments_pkey PRIMARY KEY (id);


--
-- Name: active_storage_blobs active_storage_blobs_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.active_storage_blobs
    ADD CONSTRAINT active_storage_blobs_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: articles articles_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);


--
-- Name: banners banners_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.banners
    ADD CONSTRAINT banners_pkey PRIMARY KEY (id);


--
-- Name: portfolio_items portfolio_items_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.portfolio_items
    ADD CONSTRAINT portfolio_items_pkey PRIMARY KEY (id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: shops shops_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.shops
    ADD CONSTRAINT shops_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_active_storage_attachments_on_blob_id; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX index_active_storage_attachments_on_blob_id ON public.active_storage_attachments USING btree (blob_id);


--
-- Name: index_active_storage_attachments_uniqueness; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX index_active_storage_attachments_uniqueness ON public.active_storage_attachments USING btree (record_type, record_id, name, blob_id);


--
-- Name: index_active_storage_blobs_on_key; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX index_active_storage_blobs_on_key ON public.active_storage_blobs USING btree (key);


--
-- Name: index_shops_on_shopify_domain; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX index_shops_on_shopify_domain ON public.shops USING btree (shopify_domain);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: active_storage_attachments fk_rails_c3b3935057; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT fk_rails_c3b3935057 FOREIGN KEY (blob_id) REFERENCES public.active_storage_blobs(id);


--
-- PostgreSQL database dump complete
--

