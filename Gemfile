source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.4'

# authentication with Devise 
gem 'devise', '~> 4.7', '>= 4.7.1'

# admin interface
gem 'rails_admin', '~> 2.0', '>= 2.0.1'

# Mounting React
gem 'webpacker', '~> 4.2'
gem 'foreman', '~> 0.86.0'
gem 'react_on_rails', '~> 11.3'

gem 'bundler'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.3'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
gem 'passenger', '~> 6.0', '>= 6.0.2'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# Convert hash/ActiveRecord objects into JSON
gem 'json', '~> 2.3'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'irb', '~> 1.0'
# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'


# Bootstrap
gem 'bootstrap', '~> 4.3.1'
gem 'sprockets', '~> 3.7', '>= 3.7.2'
gem 'popper_js', '~> 1.14.5'
gem 'jquery-rails'
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.5'
gem 'font-mfizz-rails', '~> 1.1', '>= 1.1.2'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'httparty', '~> 0.17.0'
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Create db schema diagram 
  gem 'rails-erd', '~> 1.6'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'dotenv-rails', group: [:test, :development]

gem 'mini_racer', platforms: :ruby
