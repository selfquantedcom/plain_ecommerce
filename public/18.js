var htmlString = `
   <div>
   <div class='plain-banner'>
   <div class='plain-close-btn' onclick='closeBanner()'>X</div>
   <div class='plain-banner-body'>
   <div>
   <span class='plain-banner-body-text'>
   <h1>title </h1>
   <p>some text</p>
   </span>
   </div>
   </div>
   </div>
   </div>`;

   var style = `<style type='text/css'>
   .plain-banner{
    position: fixed;
    bottom:0;
    right:0;
    height: 200px;
    width: 25%;
    overflow: hidden;
    background: #ff0000;
    border-radius: 25px;
    z-index: 99999;
  }

  .plain-close-btn{
   z-index: 9999;
   float: right;   
   padding-right: 20px;
   padding-top: 10px;
   font-size: 20px;
   cursor: pointer;
 }

 .plain-banner-body{ 
  z-index: -1;
  text-align: center;
  position:  absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  color: white;
}
.plain-banner-body div { 
  display: table;
  width: 100%;
  height: 100%;
} 
.plain-banner-body-text { 
  display: table-cell;
  text-align: center;
  vertical-align: middle;
  color: black;
} 
</style>`;

var scriptInnerHTML = `   
function closeBanner(){
 var child = document.querySelector('.plain-close-btn').parentElement
 child.parentElement.removeChild(child)       
}   
`
var head = document.querySelector('head');
var script = document.createElement('script');
script.innerHTML = scriptInnerHTML;
head.insertAdjacentHTML('beforeend', style);  
head.append(script);    
document.querySelector('body').insertAdjacentHTML('afterbegin', htmlString);
